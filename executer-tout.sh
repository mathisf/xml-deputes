#!/bin/bash

# Cree le dossier de sortie (si il n'existe pas deja)
mkdir -p sortie;

read -p "Entrez le chemin vers saxon (saxon9he.jar) : " chemin_saxon
if [ -z "$chemin_saxon" ];  then
    chemin_saxon="saxon9he.jar"
fi

# Execute tous les scripts
echo "Exécution de transformation-dom-sans-xpath.php :"
time php transformation-dom-sans-xpath.php > sortie/resultat-dom-sans-xpath.xml
echo
echo "Exécution de transformation-dom-xpath.php :"
time php transformation-dom-xpath.php > sortie/resultat-dom-xpath.xml
echo 
echo "Exécution de transformation-sax.php :"
time php transformation-sax.php > sortie/resultat-sax.xml
echo
echo "Exécution de transformer-xslt-xpath1.php :"
time php transformation-xslt-xpath1.php > sortie/resultat-xslt-xpath1.xml
echo
echo "Exécution de transformation-xslt-xpath2.xsl :"
if [ "$chemin_saxon" = "saxon" ]; then
    time saxon AMO30_tous_acteurs_tous_mandats_tous_organes_historique.xml transformation-xslt-xpath2.xsl > sortie/resultat-xslt-xpath2.xml
else
    time java -jar "$chemin_saxon" -xsl:transformation-xslt-xpath2.xsl -s:AMO30_tous_acteurs_tous_mandats_tous_organes_historique.xml > sortie/resultat-xslt.xml
fi
echo
echo "Les résultats se trouvent dans le dossier \"sortie\"."