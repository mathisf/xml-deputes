<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="3.0">
    <!-- Pour XPath 2 -->
    <xsl:output indent="yes"/>

    <xsl:template match="/">
        <xsl:text disable-output-escaping="yes">
&lt;!DOCTYPE nantais 
    SYSTEM "ex.dtd"&gt;
</xsl:text>
        <nantais>
            <xsl:apply-templates
                select="/export/acteurs/acteur[etatCivil/infoNaissance/villeNais/text() eq 'Nantes' and ./mandats/mandat/infosQualite/codeQualite/text() = 'Président']">
                <xsl:sort
                    select="concat(./etatCivil/ident/nom/text(), '', ./etatCivil/ident/prenom/text())"
                />
            </xsl:apply-templates>
        </nantais>
    </xsl:template>

    <xsl:template match="acteur">
        <personne nom="{concat(./etatCivil/ident/prenom/text(), ' ', ./etatCivil/ident/nom/text())}">
            <xsl:apply-templates select="./mandats/mandat[infosQualite/codeQualite/text() = 'Président']">
            <xsl:sort
                    select="concat(./dateDebut, '', ./uid)"
                />
            </xsl:apply-templates>
        </personne>
    </xsl:template>

    <xsl:template match="mandat">
        <xsl:element name="md">
            <xsl:attribute name="code">
                <xsl:value-of select="organes/organeRef"/>
            </xsl:attribute>
            <xsl:attribute name="début">
                <xsl:value-of select="dateDebut"/>
            </xsl:attribute>
            <xsl:attribute name="legislature">
                <xsl:value-of select="legislature"/>
            </xsl:attribute>      
            <xsl:if test="dateFin/text() ne ''">
                <xsl:attribute name="fin">
                    <xsl:value-of select="dateFin"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test="datePublication/text() ne ''">
                <xsl:attribute name="pub">
                    <xsl:value-of select="datePublication"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates select="organes/organeRef">
                <xsl:with-param name="organeRef">
                    <xsl:value-of select="organes/organeRef"/>
                </xsl:with-param>
            </xsl:apply-templates>
        </xsl:element>
    </xsl:template>

    <xsl:template match="organeRef">
        <xsl:param name="organeRef"/>
        <xsl:value-of select="/export/organes/organe[uid/text() eq $organeRef]/libelle"/>
    </xsl:template>
</xsl:stylesheet>
