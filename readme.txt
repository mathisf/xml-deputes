Projet de transformation d'un fichier XML
Par Thibaut Godet et Mathis Faivre

Le fichier XML source est à mettre à la racine du dossier, il doit être nommé :
"AMO30_tous_acteurs_tous_mandats_tous_organes_historique.xml"
Utilise php (testé avec php7.1) et saxon (version Saxon-HE 9.9.0.1J et Saxon-HE 9.9.0.2J)
Le script bash d'automatisation supporte saxon sous forme de jar (avec Java) et sous forme de CLI.


---
Pour exécuter et comparer les quatres programmes de transformation :
Exécuter le programme bash 'executer-tout.sh' avec un terminal bash, par exemple : 
$ ./executer-tout.sh

Si le programme bash ne s'execute pas parce que vous n'avez pas les droits, exécuter :
$ chmod +x executer-tout.sh

---
Pour exécuter individuellement chaque programme de transformation :
$ php transformation-dom-sans-xpath.php
$ php transformation-dom-xpath.php
$ php transformation-sax.php
$ php transformation-xslt-xpath1.php
$ saxon AMO30_tous_acteurs_tous_mandats_tous_organes_historique.xml transformation-xslt-xpath2.xsl

Si saxon9he.jar est utilisé : 
$ java -jar chemin/vers/saxon9he.jar -xsl:transformation-xslt-xpath2.xsl -s:AMO30_tous_acteurs_tous_mandats_tous_organes_historique.xml

---
Pour mesurer le temps d'exécution de chaque programme de transformation :
Ajouter time avant la commande, par exemple :
$ time php transformation-dom-sans-xpath.php

Attention : La sortie du programme 'time' s'affiche dans le stderr et non le stdout

---
Pour effectuer le benchmark des différentes solutions, nous recommandons d'utiliser 'hyperfine' (https://github.com/sharkdp/hyperfine)
La commande suivante est utile :
$ hyperfine --min-runs 5 --warmup 2 "php transformation-dom-sans-xpath.php" 
    "php transformation-dom-xpath.php" 
    "php transformation-sax.php" 
    "php transformation-xslt-xpath1.php" 
    "saxon AMO30_tous_acteurs_tous_mandats_tous_organes_historique.xml transformation-xslt-xpath2.xsl"