<?php
error_reporting(0);
$doc = new DOMDocument();
$doc->validateOnParse = true;
$doc->preserveWhiteSpace = false;
$doc->load('AMO30_tous_acteurs_tous_mandats_tous_organes_historique.xml', 1);

// Tableau associatif permettant d'indexer les libelles des organes par leur organeRef
$tableauAssociatifOrganes = []; // [organeRef] = libelle
// Tablau associatif permettant d'indexer le code XML d'une personne (sous forme de string) par leur nom et prénom
$tableauAssociatifActeurs = []; // [nom + prenom] = personne xml
// Tableau associatif permettant d'indexer les mandats d'une personne par son nom, prénom et sa date
$tableauAssociatifMandats = []; // [nom + prenom + dateDebut] = md xml

// Le noeud racine du document (appelé 'export')
$x = $doc->documentElement;
// Pour chaque noeuds enfants du noeud 'export'
foreach ($x->childNodes AS $item) {
    // Si le noeud courant est appelé 'organes'
    if ($item->nodeName == "organes") {
        // Pour chaque enfants du noeud 'organes'
        foreach($item->childNodes as $organe) {
            // Pour tout les noeuds enfants d'un organe
            foreach ($organe->childNodes as $enfantOrgane) {
                // Si le nom du noeud enfant est 'uid' ou 'libelle'
                switch($enfantOrgane->nodeName) {
                    case 'uid':
                        $ref = $enfantOrgane->nodeValue;
                        break;
                    case 'libelle':
                        $libelle = $enfantOrgane->nodeValue;
                        break;
                }
            }
            // On associe l'uid avec le libelle de l'organe
            $tableauAssociatifOrganes[$ref] = $libelle;
        }
    }
    // Si le noeud courant est appelé 'acteurs'
    if ($item->nodeName == "acteurs") {
        // Pour chaque noeud enfants d'un acteur
        foreach($item->childNodes as $acteur) {
            // Si l'acteur est nantais
            if ($acteur->childNodes[1]->childNodes[1]->childNodes[1]->nodeValue == "Nantes") { 
                $mandats = $acteur->childNodes[3]->childNodes;
                $possedeUnMandatPresident = false;
                // Permet de savoir si l'acteur possède un mandat de président
                foreach ($mandats as $mandat) {
                    if ($mandat->childNodes[9]->firstChild->nodeValue == "Président") {
                        $possedeUnMandatPresident = true;
                        break;
                    }
                }
                // Si l'acteur est un président
                if ($possedeUnMandatPresident) {
                    // On récupère les données et on les stock dans les variables
                    $nom = $acteur->childNodes[1]->firstChild->childNodes[2]->nodeValue;
                    $prenom = $acteur->childNodes[1]->firstChild->childNodes[1]->nodeValue;
                    $tableauAssociatifActeurs[$nom . $prenom] = "\t".'<personne nom="' . 
                        $prenom . 
                        " " . 
                        $nom . 
                        '">' . "\n";
                    $tableauAssociatifMandats[$nom . $prenom] = [];
                    foreach ($mandats as $mandat) {
                        if ($mandat->childNodes[9]->firstChild->nodeValue == "Président") {
                            $uid = $mandat->firstChild->nodeValue;
                            $code = $mandat->childNodes[10]->firstChild->nodeValue;
                            $debut = $mandat->childNodes[4]->nodeValue;
                            $fin = $mandat->childNodes[6]->nodeValue;
                            $legislature = $mandat->childNodes[2]->nodeValue;
                            $pub = $mandat->childNodes[5]->nodeValue;
                            $tableauAssociatifMandats[$nom . $prenom][$debut . $uid] = "\t\t" . '<md code="' . $code . '" début="' . $debut . '"';
                            $tableauAssociatifMandats[$nom . $prenom][$debut . $uid] .=  $fin ? ' fin="' . $fin . '"' : '';
                            $tableauAssociatifMandats[$nom . $prenom][$debut . $uid] .=  ' legislature="' . $legislature . '"';
                            $tableauAssociatifMandats[$nom . $prenom][$debut . $uid] .=  $pub ? ' pub="' . $pub . '"' : '';
                            $tableauAssociatifMandats[$nom . $prenom][$debut . $uid] .= '>' . 
                                $tableauAssociatifOrganes[$code] . 
                                "</md>\n";
                            
                        }
                    }
                }
            }
        }
    }
}
// On affiche le résultat en triant les tableaux associatifs (ksort trie par les clés et non les valeurs)
echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . 
    '<!DOCTYPE nantais' . "\n\t" . 
    'SYSTEM "ex.dtd">' . "\n";
echo "<nantais>\n";

ksort($tableauAssociatifActeurs);
foreach ($tableauAssociatifActeurs as $nomPrenom => $personne) {
    echo $personne;
    ksort($tableauAssociatifMandats[$nomPrenom]);
    foreach ($tableauAssociatifMandats[$nomPrenom] as $mandat) {
        echo $mandat;
    }
    echo "\t</personne>\n";
}
echo "</nantais>"; 