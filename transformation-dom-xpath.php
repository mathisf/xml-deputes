<?php
// Il est bon de savoir que xpath en php retourne toujours une liste (meme quand il n'y a qu'un seul neoud)
error_reporting(0);
$doc = new DOMDocument();
$doc->validateOnParse = true;
$doc->preserveWhiteSpace = false;
$doc->load('AMO30_tous_acteurs_tous_mandats_tous_organes_historique.xml');

$xpath = new DOMXPath($doc);

// Tablau associatif permettant d'indexer le code XML d'une personne (sous forme de string) par leur nom et prénom
$tableauAssociatifActeurs = []; // [nom + prenom] = personne xml
// Tableau associatif permettant d'indexer les mandats d'une personne par son nom, prénom et sa date
$tableauAssociatifMandats = []; // [nom + prenom + dateDebut] = md xml

// On récupère les acteurs nantais et qui ont été au moins une fois président
$acteursNantaisEtPresident = $xpath->query("/export/acteurs/acteur[etatCivil/infoNaissance/villeNais/text() = 'Nantes' and count(./mandats/mandat[infosQualite/codeQualite/text() = 'Président']) > 0]");

// Pour chaque acteur nantais et qui a été président
foreach ($acteursNantaisEtPresident as $acteur) {
    // Récuperation des mandats de président de l'acteur ainsi que son nom et prénom.
    $mandats = $xpath->query("mandats/mandat[infosQualite/codeQualite/text() = 'Président']", $acteur);
    $nom = $xpath->query("etatCivil/ident/nom", $acteur)[0]->nodeValue;
    $prenom = $xpath->query("etatCivil/ident/prenom", $acteur)[0]->nodeValue;

    // Affichage du noeud 'personne'
    $tableauAssociatifActeurs[$nom . $prenom] = "\t".'<personne nom="' . 
        $prenom . 
        " " . 
        $nom . 
        '">' . "\n";
    $tableauAssociatifMandats[$nom . $prenom] = [];

    // Pour chaque mandat
    foreach ($mandats as $mandat) {
        // On récupère les données liées au mandat
        $uid = $mandat->firstChild->nodeValue;
        $code = $xpath->query("organes/organeRef", $mandat)[0]->nodeValue;
        $debut = $xpath->query("dateDebut", $mandat)[0]->nodeValue;
        $fin = $xpath->query("dateFin", $mandat)[0]->nodeValue;
        $legislature = $xpath->query("legislature", $mandat)[0]->nodeValue;
        $pub = $xpath->query("datePublication", $mandat)[0]->nodeValue;
        $tableauAssociatifMandats[$nom . $prenom][$debut . $uid] = "\t\t" . '<md code="' . $code . '" début="' . $debut . '"';
        $tableauAssociatifMandats[$nom . $prenom][$debut . $uid] .=  $fin ? ' fin="' . $fin . '"' : '';
        $tableauAssociatifMandats[$nom . $prenom][$debut . $uid] .=  ' legislature="' . $legislature . '"';
        $tableauAssociatifMandats[$nom . $prenom][$debut . $uid] .=  $pub ? ' pub="' . $pub . '"' : '';
        $tableauAssociatifMandats[$nom . $prenom][$debut . $uid] .= '>' . 
            $xpath->query("/export/organes/organe[uid/text() = '".$code."']/libelle")[0]->nodeValue . 
            "</md>\n";
    }
}

// On affiche le résultat en triant les tableaux associatifs (ksort trie par les clés et non les valeurs)
echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . 
    '<!DOCTYPE nantais' . "\n\t" . 
    'SYSTEM "ex.dtd">' . "\n";
echo "<nantais>\n";

ksort($tableauAssociatifActeurs);
foreach ($tableauAssociatifActeurs as $nomPrenom => $personne) {
    echo $personne;
    ksort($tableauAssociatifMandats[$nomPrenom]);
    foreach ($tableauAssociatifMandats[$nomPrenom] as $mandat) {
        echo $mandat;
    }
    echo "\t</personne>\n";
}
echo "</nantais>"; 